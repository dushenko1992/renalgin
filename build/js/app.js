"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//=require ../blocks/**/*.js
$(document).ready(function () {
  var $window = $(window);
  var $body = $("body");
  var $header = $(".header"); //read more

  $(function () {
    $(".example").elimore({
      moreText: "Читать далее",
      lessText: "Свернуть текст",
      maxLength: 553
    });
  });
  $(function () {
    $(".test-btn").on("click", function (e) {
      $("html,body").stop().animate({
        scrollTop: $("#test-section").offset().top
      }, 1000);
      e.preventDefault();
    });
  }); //header

  $window.on("scroll", function (e) {
    if (window.pageYOffset > 0) {
      $header.addClass("active");
    } else {
      $header.removeClass("active");
    }
  }); //burger

  if (document.querySelector(".js-burger")) {
    $(".js-burger").click(function (e) {
      e.preventDefault();
      var $this = $(this);

      if ($this.hasClass("active")) {
        $this.removeClass("active");
        $(".js-nav").removeClass("active");
        $body.removeClass("overflow");
      } else {
        $this.addClass("active");
        $(".js-nav").addClass("active");
        $body.addClass("overflow");
      }
    });
  } //footer hide


  var fixedBottom = $(".js-quality");
  $window.on("scroll", function (e) {
    if ($body.height() <= $window.height() + $window.scrollTop() + 500) {
      fixedBottom.addClass("hide");
    } else {
      fixedBottom.removeClass("hide");
    }
  }); //modal

  $(".js-open-modal").click(function (e) {
    $(".js-ask-modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  });
  $(".js-mask").click(function (e) {
    $(".js-modal").removeClass("active");
    $body.removeClass("overflow");
    $(this).removeClass("active");
  });
  $(".js-close-modal").click(function (e) {
    $(".js-modal").removeClass("active");
    $(".js-mask").removeClass("active");
    $body.removeClass("overflow");
  }); //accept

  $(".js-open-accept").click(function () {
    $(".js-accept-modal").addClass("active");
    $(".js-mask").addClass("active");
    $(".js-mask-spec").removeClass("active");
    $(".js-accept-spec").removeClass("active");
    $body.addClass("overflow");
  }); //quality

  $(".js-quality").click(function () {
    $(this).toggleClass("active").siblings().removeClass("active");
  }); //download link

  $(".nav__download").click(function () {
    $(this).toggleClass("active");
  }); //mechanik info show

  if ($(window).width() < 1024) {
    $(".js-show-info").click(function () {
      $(".js-show-info").not(this).parent().find(".js-info-abs").removeClass("active");
      $(this).parent().find(".js-info-abs").toggleClass("active");
    });
  }

  if ($(window).width() > 1024) {
    $(".js-show-info").hover(function () {
      $(this).parent().find(".js-info-abs").toggleClass("active");
    });
  } //accordion


  $(".accordion__item h3").on("click", function (e) {
    e.preventDefault();
    var $this = $(this);

    if (!$this.hasClass("active")) {
      $(".accordion__content").slideUp(400);
      $(".accordion__item h3").removeClass("active");
    }

    $this.toggleClass("active");
    $this.next().slideToggle();
  }); //tabs

  $("ul.js-tab-btn").on("click", "li:not(.active)", function () {
    $(this).addClass("active").siblings().removeClass("active").closest("div.js-tabs").find("div.js-tab-content").removeClass("active").eq($(this).index()).addClass("active").hide().stop().fadeIn();
  }); //ScrollToBottom

  var ScrollToBottom = /*#__PURE__*/function () {
    function ScrollToBottom(selector, height) {
      _classCallCheck(this, ScrollToBottom);

      this.btn = document.querySelector(selector);
      this.currentScroll = 0;
      this.scrollHeight = height;
      this.init();
    }

    _createClass(ScrollToBottom, [{
      key: "init",
      value: function init() {
        var _this = this;

        this.btn.addEventListener("click", function () {
          _this.currentScroll += _this.scrollHeight;
          window.scrollTo({
            top: _this.currentScroll,
            left: 0,
            behavior: "smooth"
          });
        });
      }
    }]);

    return ScrollToBottom;
  }();

  new ScrollToBottom(".scroll-link", 800); // Test

  (function () {
    var $container = $(".test-container");

    if (!$container.length) {
      return;
    }

    var $screens = $container.find(".microphone");
    var $startScreen = $screens.eq(0);
    var $listenScreen = $screens.eq(1);
    var $testScreen = $screens.eq(2);
    var $resultScreen = $screens.eq(3);
    var $restartButton = $resultScreen.find(".restart-btn.js-prev");
    var $circleFill = $listenScreen.find(".circleFill");
    var $circlesWrapper = $container.find(".circle-wrap");
    var $circles = $circlesWrapper.children();
    var $questions = $testScreen.find(".question");
    var $questionInputs = $questions.find("input");
    var $questionButtons = $questions.find(".js-btn-question");
    var $questionNumber = $testScreen.find(".js-question-slide");
    var $birds = $listenScreen.find(".abs-img");
    var $results = $resultScreen.find("[data-cough]");
    var audioResults;
    var questionsData;
    var $apContainer = $container.find("#audioparser");

    if (!$apContainer.length) {
      return;
    }

    AudioParser.init($apContainer[0]);
    var $apCanvas = $container.find("canvas");
    gsap.staggerFromTo($circles, 3, {
      scale: 0.75
    }, {
      scale: 1.15,
      ease: "back.out",
      repeat: -1,
      yoyo: true,
      force3D: true
    }, 0.5);

    var showScreen = function showScreen($screen, immediate) {
      $screens.css({
        "pointer-events": "none"
      }).removeClass("_opened");
      $screen.css({
        "pointer-events": "all"
      }).addClass("_opened");

      if (!immediate) {
        $screen.hide();
        $screen.stop();
        $screen.fadeIn();
      }
    };

    var birdsHided = false;

    var hideBirds = function hideBirds() {
      if (!birdsHided) {
        birdsHided = true;
        gsap.to($birds.eq(0), 1, {
          x: "-500%",
          y: "-200%",
          rotation: -720,
          autoAlpha: 0
        });
        gsap.to($birds.eq(1), 1, {
          x: "-50%",
          y: "-500%",
          rotation: 360,
          autoAlpha: 0
        });
        gsap.to($birds.eq(2), 1, {
          x: "300%",
          y: "-200%",
          rotation: 540,
          autoAlpha: 0
        });
      }
    };

    var showTest = function showTest() {
      AudioParser.stop();
      questionsData = {};
      $questions.stop().hide().first().show();
      $questionInputs.val(""); // Нужно снимать выделение с радиобаттонов

      $questionNumber.html("1 ");
      gsap.to($apCanvas, 0.35, {
        alpha: 0
      });
      showScreen($testScreen);
    };

    AudioParser.setEventsHandler(function (type, data) {
      if (type == "error") {
        console.warn(data); // нужно показать что не получается захватить звук и перейти к экрану вопросов
      } else if (type == "start") {
        //hideBirds();
        var fillController = {
          value: 0
        };
        gsap.fromTo(fillController, 5, {
          value: 0
        }, {
          value: 1,
          onUpdate: function onUpdate() {
            $circleFill[0].style["stroke-dashoffset"] = 160 - fillController.value * 160;
          }
        });
      } else if (type == "success") {
        audioResults = data;
        showTest();
      } else if (type == "activity") {
        hideBirds();
      }
    });
    $startScreen.find('[id="start"]').on("click", function (e) {
      e.preventDefault();
      audioResults = null;
      AudioParser.start();
      showScreen($listenScreen, true);
    });
    $startScreen.find('[id="slide3"]').on("click", function (e) {
      e.preventDefault();
      AudioParser.stop();
      audioResults = null;
      showTest();
      hideBirds();
    });
    $listenScreen.find("button").on("click", function (e) {
      e.preventDefault();
      showTest();
    });
    $restartButton.on("click", function (e) {
      showScreen($startScreen);
      gsap.to($apCanvas, 0.35, {
        alpha: 1
      });
      birdsHided = false;
      gsap.set($birds, {
        autoAlpha: 1,
        x: "0%",
        y: "-100%",
        rotation: 0
      });
    });
    $questionButtons.on("click", function (e) {
      e.preventDefault();
      var $this = $(this);
      var $parent = $this.parents(".question");
      var $input = $parent.find("input").filter(":checked");

      if ($input.length) {
        var inputValue = $input.attr("data-question-value");
        questionsData[$input.attr("data-question-name")] = inputValue;
        var index = $questions.index($parent);
        $questionNumber.html(index + 2 + " ");
        console.log(index, inputValue);

        if (index == 2 && inputValue == "pain-no") {
          index++;
        }

        if (index == 3 && inputValue == "character-yes") {
          index += 10;
        }

        if (index == 3 && questionsData["pain"] == "pain-yes") {
          index += 10;
        }

        if (index < $questions.length - 1) {
          $parent.hide();
          $questions.eq(index + 1).fadeIn();
        } else {
          console.log(audioResults);
          console.log(questionsData);
          var result = 0;

          if (questionsData.pain == "pain-no") {
            if (questionsData.phlegm == "phlegm-no") {
              result = 1;
            } else if (questionsData.phlegm == "phlegm-difficult") {
              result = 0;
            } else {
              result = 2;
            }
          } else {
            result = 1;
          }

          if (questionsData.phlegm == "phlegm-no") {
            result = 1;
          }

          if (audioResults) {
            if (audioResults.result != result) {
              result = 0;
            }
          }

          var resultName = result == 0 ? "doctor" : result == 1 ? "dry" : "wet";
          $results.hide().filter('[data-cough="' + resultName + '"]').show();
          console.log("Final: " + result);
          showScreen($resultScreen);
        }
      }
    });
  })(); //video


  var videoOne = $("#video-one").get(0);
  var videoTwo = $("#video-two").get(0);
  $(".js-video-open").click(function () {
    $(this).parent().find(".js-video-modal").addClass("active");
    $(this).parent().find(".js-video").trigger("play");
    $(".js-video-mask").addClass("active");
    $body.addClass("overflow");
  });
  $(".js-video-close").click(function () {
    $(".js-video-modal").removeClass("active");
    $(".js-video-mask").removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
  }); //close video onclick mask

  $(".js-video-mask").click(function () {
    $(".js-video-modal").removeClass("active");
    $(this).removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
  }); //ScrollToBottomMob

  if ($(".scroll-link-mob").length) {
    var ScrollToBottomMob = /*#__PURE__*/function () {
      function ScrollToBottomMob(selector, height) {
        _classCallCheck(this, ScrollToBottomMob);

        this.btn = document.querySelector(selector);
        this.currentScroll = 0;
        this.scrollHeight = height;
        this.init();
      }

      _createClass(ScrollToBottomMob, [{
        key: "init",
        value: function init() {
          var _this2 = this;

          this.btn.addEventListener("click", function () {
            _this2.currentScroll += _this2.scrollHeight;
            window.scrollTo({
              top: _this2.currentScroll,
              left: 0,
              behavior: "smooth"
            });
          });
        }
      }]);

      return ScrollToBottomMob;
    }();

    new ScrollToBottomMob(".scroll-link-mob", 550);
  } else {
    $(".header").addClass("not");
  }

  ;
});